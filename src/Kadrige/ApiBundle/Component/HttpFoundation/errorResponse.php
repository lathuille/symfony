<?php

namespace Kadrige\ApiBundle\Component\HttpFoundation;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseHttp;

class errorResponse extends JsonResponse
{
    /**
     * @Get("", name="")
     * @View(statusCode=200, serializerGroups={"simple_template"})
     * @param Request $request
     * @return array | Exception
     */
    public function __construct($message = "An error has occured", $status = 400, $headers = array())
    {
        $data = [
            "status"          => "error",
            "status_code"     => $status,
            "status_text"     => ResponseHttp::$statusTexts[$status],
            "current_content" =>"",
            "message"         => $message
        ];
        parent::__construct($data, $status, $headers = array());
    }


}
