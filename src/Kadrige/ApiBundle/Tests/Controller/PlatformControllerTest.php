<?php
namespace Kadrige\ApiBundle\Tests\Controller;
use Kadrige\ApiBundle\Tests\Controller\BaseTestCase as WebTestCase;

class PlatformControllerTest extends WebTestCase
{
    /**
     * checks success when getting entities
     * @access public
     */
    public function testgetPlatforms_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_platforms_get_platforms'),
            array(),
            array(),
            $this->headers
        );
        $this->assertJsonResponse(200, $this->client->getResponse());

        $data = json_decode($this->client->getResponse()->getContent());
        $this->assertCount(5, $data);
    }

    /**
     * checks success when getting entities with a right filter param
     * @access public
     */
    public function testgetPlatformsFilteredByClient_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_platforms_get_platforms'),
            array("client" => "BMS"),
            array(),
            $this->headers
        );
        $this->assertJsonResponse(200, $this->client->getResponse());

        $data = json_decode($this->client->getResponse()->getContent());
        $this->assertCount(2, $data);
    }

    /**
     * checks success when getting entities with a wrong filter param
     * @access public
     */
    public function testgetPlatformsFilteredByClient_fail()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_platforms_get_platforms'),
            array("client" => "unknown"),
            array(),
            $this->headers
        );
        $this->assertJsonResponse(200, $this->client->getResponse());

        $data = json_decode($this->client->getResponse()->getContent());
        $this->assertCount(0, $data);
    }

    /**
     * checks success when getting a single entity with right param
     * @access public
     */
    public function testgetPlatform_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_platforms_get_platform',['id'=>1]),
            array(),
            array(),
            $this->headers
        );
        $this->assertJsonResponse(200, $this->client->getResponse());

        $data = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(1, count($data));
    }

    /**
     * checks success when getting a single entity with wrong param
     * @access public
     */
    public function testgetPlatform_fail()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_platforms_get_platform',['id'=>"unknown"]),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(500, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with right params
     * @access public
     */
    public function testpostPlatform_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('POST', $this->router->generate('api_platforms_post_platform'),
        array (
          'plateforme_id' => '235',
          'name' => 'https://visit-dandelion.com/',
          'client' => 'Dandelion'
        ),
            array(),
            $this->headers
        );
        $this->assertEquals(201, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with missing params
     * @access public
     */
    public function testpostPlatform_failMissingParams()
    {
        $this->setUp(true);
        $crawler = $this->client->request('POST', $this->router->generate('api_platforms_post_platform'),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with missing params
     * @access public
     */
    public function testpostPlatform_failWrongParams()
    {
        $this->setUp(true);
        $crawler = $this->client->request('POST', $this->router->generate('api_platforms_post_platform'),
        array (
         'plateforme_id' => '235sdfsdfsdsdf',
         'name' => 'visit-dandelion.com.net',
         'client' => 'Dandelion'
        ),
            array(),
            $this->headers
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to update a entity with right params
     * @access public
     */
    public function testputPlatform_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('PUT', $this->router->generate('api_platforms_put_platform',['id'=>1]),
        array (
         'plateforme_id' => '235',
         'name' => 'https://visit-dandelion.com/',
         'client' => 'Dandelion'
       ),
            array(),
            $this->headers
        );
        $this->assertEquals(201, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with missing params
     * @access public
     */
    public function testputTemplate_failMissingParams()
    {
        $this->setUp(true);
        $crawler = $this->client->request('PUT', $this->router->generate('api_platforms_put_platform',['id'=>1]),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with missing params
     * @access public
     */
    public function testputPlatform_failWrongParams()
    {
        $this->setUp(true);
        $crawler = $this->client->request('PUT', $this->router->generate('api_platforms_put_platform',['id'=>'1']),
        array (
        'plateforme_id' => '235dsfdsfsfsf',
        'name' => 'visit-dandelion.com.net',
        'client' => 'Dandelion'
       ),
            array(),
            $this->headers
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }


    /**
     * checks success when trying to delete a created entity
     * @access public
     */
    public function testDeletePlatform_Success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('DELETE', $this->router->generate('api_platforms_delete_platform',['id'=>'1']),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }


    /**
     * checks success when trying to delete a entity which has been not stored in database
     * @access public
     */
    public function testDeletePlatform_Fail()
    {
        $this->setUp(true);
        $crawler = $this->client->request('DELETE', $this->router->generate('api_platforms_delete_platform',['id'=>'unknown']),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    /**
     * checks success when trying to delete a entity which has been not stored in database
     * @access public
     */
    public function testDeletePlatforms_Success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('DELETE', $this->router->generate('api_platforms_delete_platforms',['ids'=>'1,2']),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }

    /**
     * checks success when trying to delete a entity which has been not stored in database
     * @access public
     */
    public function testDeletePlatforms_Fail()
    {
        $this->setUp(true);
        $crawler = $this->client->request('DELETE', $this->router->generate('api_platforms_delete_platforms'),
            array("ids" => "1,2"),
            array(),
            $this->headers
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }


}
