<?php

namespace Kadrige\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

//use Liip\FunctionalTestBundle\Test\WebTestCase as LiipWebTestCase;

abstract class BaseTestCase extends WebTestCase
{
    protected $application;
    protected $router;
    protected $client;
    protected $headers = array();

    /**
      * class constructor
      * @access public
      */
    public function setUp($loadFixture = false)
    {
        $this->client = static::createClient();
        $this->getApplication()->setAutoExit(false);

        $this->runCommand('doctrine:schema:drop --env=test --force');
        $this->runCommand('doctrine:schema:update --env=test --force');
        if ($loadFixture === true){
           $this->runCommand("doctrine:fixtures:load --fixtures=src/Kadrige/ApiBundle/DataFixtures --env=test --append");
        }

        $this->router = $this->client->getContainer()->get('router');
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->application->getKernel()->getContainer();
    }

    protected function runCommand($command)
    {
       $command = sprintf('%s --quiet -e=test', $command);
       return $this->application->run(new StringInput($command));
    }

    /**
     * @return Application
     */
    protected function getApplication()
    {
        if (null === $this->application) {
            $this->application = new Application($this->client->getKernel());
            $this->application->setAutoExit(false);
        }

        return $this->application;
    }

    /**
     * @return void
     */
    protected function assertKeysInArray(array $keys, array $array){
        foreach($keys as $key){
            $this->assertArrayHasKey($key, $array, 'Problem: Missing Exposed field : '.$key);
        }
    }

    /**
     * Check response and json
     */
    protected function assertJsonResponse($statusCode = 200, $response)
    {
       $this->assertEquals(
           $statusCode, $response->getStatusCode(),
           $response->getContent()
       );

       $this->assertTrue(
           $response->headers->contains('Content-Type', 'application/json'),
           $response->headers
       );
    }

}
