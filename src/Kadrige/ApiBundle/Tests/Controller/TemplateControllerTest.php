<?php
namespace Kadrige\ApiBundle\Tests\Controller;

use Kadrige\ApiBundle\Tests\Controller\BaseTestCase as WebTestCase;

class TemplateControllerTest extends WebTestCase
{
    /**
     * checks success when getting entities
     * @access public
     */
    public function testgetTemplates_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_templates_get_templates'),
            array(),
            array(),
            $this->headers
        );
        $this->assertJsonResponse(200, $this->client->getResponse());

        $data = json_decode($this->client->getResponse()->getContent());
        $this->assertCount(3, $data);
    }

    /**
     * checks success when getting entities with a right filter param
     * @access public
     */
    public function testgetTemplatesFilteredByClient_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_templates_get_templates'),
            array("client" => "BMS"),
            array(),
            $this->headers
        );
        $this->assertJsonResponse(200, $this->client->getResponse());

        $data = json_decode($this->client->getResponse()->getContent());
        $this->assertCount(3, $data);
    }

    /**
     * checks success when getting entities with a wrong filter param
     * @access public
     */
    public function testgetTemplatesFilteredByClient_fail()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_templates_get_templates'),
            array("client" => "unknown"),
            array(),
            $this->headers
        );
        $this->assertJsonResponse(200, $this->client->getResponse());

        $data = json_decode($this->client->getResponse()->getContent());
        $this->assertCount(0, $data);
    }

    /**
     * checks success when getting a single entity with right param
     * @access public
     */
    public function testgetTemplate_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_templates_get_template',['id'=>1]),
            array(),
            array(),
            $this->headers
        );
        $this->assertJsonResponse(200, $this->client->getResponse());

        $data = json_decode($this->client->getResponse()->getContent());
        $this->assertEquals(1, count($data));
    }

    /**
     * checks success when getting a single entity with wrong param
     * @access public
     */
    public function testgetTemplate_fail()
    {
        $this->setUp(true);
        $crawler = $this->client->request('GET', $this->router->generate('api_templates_get_template',['id'=>"unknown"]),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(500, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with right params
     * @access public
     */
    public function testpostTemplate_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('POST', $this->router->generate('api_templates_post_templates'),
        array (
          'user_id' => 'raymond@kadrige.com',
          'plateforme' =>
          array (
            array (
              'plateforme_id' => '155',
              'client' => 'amgen',
              'plateforme_name' => 'https://appremote-dev.qa.keo.net/',
            ),
            array (
              'plateforme_id' => '32',
              'client' => 'amgen',
              'plateforme_name' => 'https://appremote2-dev.qa.keo.net/',
            ),
            array (
              'plateforme_id' => '124',
              'client' => 'BMS',
              'plateforme_name' => 'https://appremote3-dev.qa.keo.net/',
            )
          ),
          'disclaimer' =>
          array (
            array (
              'prefix' => 'en_US',
              'content' => '<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer</p>',
            ),
            array (
              'prefix' => 'fr_FR',
              'content' => '<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer</p>',
            )
          ),
          'adresse' => 'adresse adresse adresse adresse adresse adresse ',
          'url' => 'http://www.kadrige.com'
        ),
            array(),
            $this->headers
        );
        $this->assertEquals(201, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with missing params
     * @access public
     */
    public function testpostTemplate_failMissingParams()
    {
        $this->setUp(true);
        $crawler = $this->client->request('POST', $this->router->generate('api_templates_post_templates'),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with missing params
     * @access public
     */
    public function testpostTemplate_failWrongParams()
    {
        $this->setUp(true);
        $crawler = $this->client->request('POST', $this->router->generate('api_templates_post_templates'),
        array (
          'user_id' => 'raymond@kadrige@com',
          'plateforme' =>
          array (
            array (
              'plateforme_id' => '155',
              'client' => 'amgen',
              'plateforme_name' => 'https://appremote-dev.qa.keo.net/',
            ),
            array (
              'plateforme_id' => '32',
              'client' => 'amgen',
              'plateforme_name' => 'https://appremote2-dev.qa.keo.net/',
            ),
            array (
              'plateforme_id' => '124',
              'client' => 'BMS',
              'plateforme_name' => 'https://appremote3-dev.qa.keo.net/',
            )
          ),
          'disclaimer' =>
          array (
            array (
              'prefix' => 'en_US',
              'content' => '<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer</p>',
            ),
            array (
              'prefix' => 'fr_FR',
              'content' => '<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer</p>',
            )
          ),
          'adresse' => 'adresse adresse adresse adresse adresse adresse ',
          'url' => 'http://www.kadrige.com'
        ),
            array(),
            $this->headers
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to update a entity with right params
     * @access public
     */
    public function testputTemplate_success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('PUT', $this->router->generate('api_templates_put_template',['id'=>1]),
        array (
          'user_id' => 'raymond@kadrige.com',
          'plateforme' =>
          array (
            array (
              'plateforme_id' => '155',
              'client' => 'amgen',
              'plateforme_name' => 'https://appremote-dev.qa.keo.net/',
            ),
            array (
              'plateforme_id' => '32',
              'client' => 'amgen',
              'plateforme_name' => 'https://appremote2-dev.qa.keo.net/',
            ),
            array (
              'plateforme_id' => '124',
              'client' => 'BMS',
              'plateforme_name' => 'https://appremote3-dev.qa.keo.net/',
            )
          ),
          'disclaimer' =>
          array (
            array (
              'prefix' => 'en_US',
              'content' => '<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer</p>',
            ),
            array (
              'prefix' => 'fr_FR',
              'content' => '<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer</p>',
            )
          ),
          'adresse' => 'adresse adresse adresse adresse adresse adresse ',
          'url' => 'http://www.kadrige.com'
        ),
            array(),
            $this->headers
        );
        $this->assertEquals(201, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with missing params
     * @access public
     */
    public function testputTemplate_failMissingParams()
    {
        $this->setUp(true);
        $crawler = $this->client->request('PUT', $this->router->generate('api_templates_put_template',['id'=>1]),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatuscode());
    }

    /**
     * checks success when trying to create a entity with missing params
     * @access public
     */
    public function testputTemplate_failWrongParams()
    {
        $this->setUp(true);
        $crawler = $this->client->request('PUT', $this->router->generate('api_templates_put_template',['id'=>1]),
        array (
          'user_id' => 'raymond@kadrige@com',
          'plateforme' =>
          array (
            array (
              'plateforme_id' => '155',
              'client' => 'amgen',
              'plateforme_name' => 'https://appremote-dev.qa.keo.net/',
            ),
            array (
              'plateforme_id' => '32',
              'client' => 'amgen',
              'plateforme_name' => 'https://appremote2-dev.qa.keo.net/',
            ),
            array (
              'plateforme_id' => '124',
              'client' => 'BMS',
              'plateforme_name' => 'https://appremote3-dev.qa.keo.net/',
            )
          ),
          'disclaimer' =>
          array (
            array (
              'prefix' => 'en_US',
              'content' => '<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer</p>',
            ),
            array (
              'prefix' => 'fr_FR',
              'content' => '<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer</p>',
            )
          ),
          'adresse' => 'adresse adresse adresse adresse adresse adresse ',
          'url' => 'http://www.kadrige.com'
        ),
            array(),
            $this->headers
        );
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }


    /**
     * checks success when trying to delete a created entity
     * @access public
     */
    public function testDeleteTemplate_Success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('DELETE', $this->router->generate('api_templates_delete_template',['id'=>1]),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }


    /**
     * checks success when trying to delete a entity which has been not stored in database
     * @access public
     */
    public function testDeleteTemplate_Fail()
    {
        $this->setUp(true);
        $crawler = $this->client->request('DELETE', $this->router->generate('api_templates_delete_template',['id'=>99]),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }

    /**
     * checks success when trying to delete a entity which has been not stored in database
     * @access public
     */
    public function testDeleteTemplates_Success()
    {
        $this->setUp(true);
        $crawler = $this->client->request('DELETE', $this->router->generate('api_templates_delete_templates',['ids'=>'1,2']),
            array(),
            array(),
            $this->headers
        );
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }

    /**
     * checks success when trying to delete a entity which has been not stored in database
     * @access public
     */
    public function testDeleteTemplates_Fail()
    {
        $this->setUp(true);
        $crawler = $this->client->request('DELETE', $this->router->generate('api_templates_delete_templates'),
                array("ids" => "1,2"),
            array(),
            $this->headers
        );
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }


}
