<?php

namespace Kadrige\ApiBundle\Model;

use Doctrine\ORM\EntityManager;

interface TemplateInterface {

  public function setIdUser($idUser);
  public function getIdUser();
  public function setClient($client);
  public function getClient();
  public function setIdPlateforme($idPlateforme);
  public function getIdPlateforme();
  public function setName($name);
  public function getName();
  public function setBranchName($branchName);
  public function getBranchName();
  public function setDateCreation($dateCreation);
  public function getDateCreation();
  public function setDisclaimer($disclaimer);
  public function getDisclaimer();
  public function setAdresse($adresse);
  public function getAdresse();
  public function setUrl($url);
  public function getUrl();
  public function getId();

}
