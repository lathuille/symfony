<?php

namespace Kadrige\ApiBundle\Controller;

use Kadrige\ApiBundle\Entity\Platform;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Kadrige\ApiBundle\Component\HttpFoundation\errorResponse;

use FOS\RestBundle\Controller\Annotations\View;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;

class PlatformController extends Controller
{
    /**
     * @QueryParam(name="client", requirements="[a-zA-Z-]+", description="platform client name.")
     * @QueryParam(name="plateforme_id", description="platform mpa id.")
     * @QueryParam(name="sort", requirements="(name|dateCreation)", default="dateCreation", description="Sort")
     * @QueryParam(name="sortorder", requirements="(asc|desc)", allowBlank=false, default="desc", description="Sort direction")
     *
     * @Get("", name="")
     * @View(statusCode=200, serializerGroups={"simple_platform"})
     * @param Request $request
     * @return array | Exception
     */
    public function getPlatformsAction(Request $request)
    {
      try {
            $platforms = $this->container
              ->get('kadrige_api.platform.handler')
              ->findBy($request->query->all());

            return $platforms;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }

    }

    /**
     * @Get("/{id}", name="")
     * @View(statusCode=200, serializerGroups={"extended_platform"})
     * @param mixed   $id
     * @return object | Exception
     */
    public function getPlatformAction($id)
    {
        $platform = $this->container
          ->get('kadrige_api.platform.handler')
          ->get($id);

        if(!$platform instanceof Platform) {
           throw new HttpException('Platform not found', Response::HTTP_NOT_FOUND);
        }

        return $platform;
    }

    /**
     * @View(statusCode=201, serializerGroups={"simple_platform"})
     * @Post("")
     *
     * @RequestParam(name="name", requirements="\w+", description="userid uid", strict=true)
     * @RequestParam(name="client", requirements="[a-zA-Z.-]+", description="platform owner", strict=true)
     * @RequestParam(name="plateforme_id",requirements="\d+", description="platform id", strict=true)
     *
     * @param Request $request
     * @return object | Exception
     */
    public function postPlatformAction(Request $request)
    {
        try {
            $platform = $this->container
              ->get('kadrige_api.platform.handler')
              ->post($request->request->all(), new Platform());

            return $platform;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }

    /**
     * update the platform record
     * @View(statusCode=201, serializerGroups={"simple_platform"})
     * @Put("/{id}")
     *
     * @RequestParam(name="name", requirements="\w+", description="platform name", strict=true)
     * @RequestParam(name="client", requirements="[a-zA-Z.-]+", description="platform client name", strict=true)
     * @RequestParam(name="plateforme_id",requirements="\d+", description="platform mpa id", strict=true)
     *
     * @param Request $request
     * @param integer $id
     * @return object | Exception
     */
    public function putPlatformAction(Request $request, $id)
    {
        try {
          $platform = $this->container
              ->get('kadrige_api.platform.handler')
              ->get($id);

            if (!$platform instanceof Platform) {
               throw new HttpException('Platform not found', Response::HTTP_NOT_FOUND);
            }

            $platform = $this->container
                ->get('kadrige_api.platform.handler')
                ->put($request->request->all(), $platform);

            return $platform;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }

    /**
     * @View(statusCode=204, serializerGroups={"simple_platform"})
     * @Delete("/{id}")
     *
     * @param integer $id
     * @return object | Exception
     */
    public function deletePlatformAction($id)
    {
        try {
          $platform = $this->container
              ->get('kadrige_api.platform.handler')
              ->get($id);

            if (!$platform instanceof Platform) {
               throw new HttpException('Platform not found or already deleted.', Response::HTTP_NOT_FOUND);
            }
            $em = $this->getDoctrine()->getManager();
            $em->remove($platform);
            $em->flush();

            return true;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }

    /**
     * @View(statusCode=204, serializerGroups={"simple_platform"})
     * @Delete("")
     * @RequestParam(name="ids", requirements="[0-9,]+", description="template ids", strict=true)
     *
     * @param Request $request
     * @return object | Exception
     */
    public function deletePlatformsAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();

            $aIds = explode(",",$request->request->get('ids'));

            foreach ($aIds as $id) {
                $this->deletePlatformAction($id);
            }
            $em->getConnection()->commit();
            die();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        } catch (HttpException $e) {
            $em->getConnection()->rollback();
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }


}
