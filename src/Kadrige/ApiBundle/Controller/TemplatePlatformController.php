<?php

namespace Kadrige\ApiBundle\Controller;

use Kadrige\ApiBundle\Entity\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Kadrige\ApiBundle\Component\HttpFoundation\errorResponse;

use FOS\RestBundle\Controller\Annotations\View;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Collections\ArrayCollection;

class TemplateController extends Controller
{
    /**
     * @QueryParam(name="client", requirements="[a-zA-Z-]+", description="platform client in which the template shouldbe deployed.")
     * @QueryParam(name="plateforme_id", description="platform id in which the template shouldbe deployed.")
     * @QueryParam(name="user_id", description="user id who creates the template")
     * @QueryParam(name="sort", requirements="(name|dateCreation)", default="dateCreation", description="Sort")
     * @QueryParam(name="sortorder", requirements="(asc|desc)", allowBlank=false, default="desc", description="Sort direction")
     *
     * @Get("", name="")
     * @View(statusCode=200, serializerGroups={"simple_template"})
     * @param Request $request
     * @return array | Exception
     */
    public function getTemplatesAction(Request $request)
    {
      try {
            $templates = $this->container
              ->get('kadrige_api.template.handler')
              ->findBy($request->query->all());

            return $templates;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }

    }

    /**
     * @Get("/{id}", name="")
     * @View(statusCode=200, serializerGroups={"extended_template"})
     * @param mixed   $id
     * @return object | Exception
     */
    public function getTemplateAction($id)
    {
        $template = $this->container
          ->get('kadrige_api.template.handler')
          ->get($id);

        if(!$template instanceof Template) {
           throw new HttpException('Template not found', Response::HTTP_NOT_FOUND);
        }

        return $template;
    }

    /**
     * @View(statusCode=201, serializerGroups={"simple_template"})
     * @Post("")
     *
     * @RequestParam(name="user_id", requirements="\w+", description="userid uid", strict=true)
     * @RequestParam(name="client", requirements="[a-zA-Z.-]+", description="platform owner", strict=true)
     * @RequestParam(name="plateforme_id",requirements="\d+", description="platform id", strict=true)
     * @RequestParam(name="disclaimer", requirements="[a-zA-Z.-]+", description="disclaimer", strict=true)
     * @RequestParam(name="adresse", requirements="[a-zA-Z-]+", description="adresse", strict=true)
     * @RequestParam(name="url",requirements="[a-zA-Z-]+", description="url", strict=true)
     * @RequestParam(name="color",requirements="[0-9#]+", description="color", strict=true)
     * @RequestParam(name="colorb",requirements="[0-9#]+", description="colorb", strict=true)
     *
     * @param Request $request
     * @return object | Exception
     */
    public function postTemplatesAction(Request $request)
    {
        try {
            $template = $this->container
              ->get('kadrige_api.template.handler')
              ->post($request->request->all(), new Template());

            return $template;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }

    /**
     * update the template record without pushing it on the remote branch
     * @View(statusCode=201, serializerGroups={"simple_template"})
     * @Put("/{id}")
     *
     * @RequestParam(name="user_id", requirements="\w+", description="userid uid", strict=true)
     * @RequestParam(name="client", requirements="[a-zA-Z.-]+", description="platform owner", strict=true)
     * @RequestParam(name="plateforme_id",requirements="\d+", description="platform id", strict=true)
     * @RequestParam(name="disclaimer", requirements="[a-zA-Z.-]+", description="disclaimer", strict=true)
     * @RequestParam(name="adresse", requirements="[a-zA-Z-]+", description="adresse", strict=true)
     * @RequestParam(name="url",requirements="[a-zA-Z-]+", description="url", strict=true)
     * @RequestParam(name="color",requirements="[0-9#]+", description="color hexa code", strict=true)
     * @RequestParam(name="colorb",requirements="[0-9#]+", description="color hexa code", strict=true)
     *
     * @param Request $request
     * @param integer $id
     * @return object | Exception
     */
    public function putTemplateAction(Request $request, $id)
    {
        try {
          $template = $this->container
              ->get('kadrige_api.template.handler')
              ->get($id);

            if (!$template instanceof Template) {
               throw new HttpException('Template not found', Response::HTTP_NOT_FOUND);
            }

            $template = $this->container
                ->get('kadrige_api.template.handler')
                ->put($request->request->all(), $template);

            return $template;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }

    /**
     * update the template record, push it on the remote branch without calling the remote kid ws
     * @View(statusCode=201, serializerGroups={"simple_template"})
     * @Put("/{id}/push")
     *
     * @RequestParam(name="user_id", requirements="\w+", description="userid uid", strict=true)
     * @RequestParam(name="client", requirements="[a-zA-Z.-]+", description="platform owner", strict=true)
     * @RequestParam(name="plateforme_id",requirements="\d+", description="platform id", strict=true)
     * @RequestParam(name="disclaimer", requirements="[a-zA-Z.-]+", description="disclaimer", strict=true)
     * @RequestParam(name="adresse", requirements="[a-zA-Z-]+", description="adresse", strict=true)
     * @RequestParam(name="url",requirements="[a-zA-Z-]+", description="url", strict=true)
     * @RequestParam(name="color",requirements="[0-9#]+", description="color hexa code", strict=true)
     * @RequestParam(name="colorb",requirements="[0-9#]+", description="color hexa code", strict=true)
     *
     * @param Request $request
     * @param integer $id
     * @return object | Exception
     */
    public function pushTemplateAction(Request $request, $id)
    {
        try {
          $template = $this->container
              ->get('kadrige_api.template.handler')
              ->get($id);

            if (!$template instanceof Template) {
               throw new HttpException('Template not found', Response::HTTP_NOT_FOUND);
            }

            $template = $this->container
                ->get('kadrige_api.template.handler')
                ->put($request->request->all(), $template);
            $this->container
                ->get('kadrige_api.repository.handler')
                ->saveCustom($template);

            return $template;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }

     /**
      * update the template record, push it on the remote branch and call the remote platform to pull the remote branch
      * @View(statusCode=201, serializerGroups={"simple_template"})
      * @Put("/{id}/release")
      *
      * @RequestParam(name="user_id", requirements="\w+", description="userid uid", strict=true)
      * @RequestParam(name="client", requirements="[a-zA-Z.-]+", description="platform owner", strict=true)
      * @RequestParam(name="plateforme_id",requirements="\d+", description="platform id", strict=true)
      * @RequestParam(name="disclaimer", requirements="[a-zA-Z.-]+", description="disclaimer", strict=true)
      * @RequestParam(name="adresse", requirements="[a-zA-Z-]+", description="adresse", strict=true)
      * @RequestParam(name="url",requirements="[a-zA-Z-]+", description="url", strict=true)
      * @RequestParam(name="color",requirements="[0-9#]+", description="color hexa code", strict=true)
      * @RequestParam(name="colorb",requirements="[0-9#]+", description="color hexa code", strict=true)
      * @RequestParam(name="platformdest",requirements="[a-zA-Z-]+", description="dest platform url", strict=true)
      *
      * @param Request $request
      * @param integer $id
      * @return object | Exception
      */
    public function releaseTemplateAction(Request $request, $id)
    {
        try {
            $template = $this->container
              ->get('kadrige_api.template.handler')
              ->get($id);

            if (!$template instanceof Template) {
               throw new HttpException('Template not found', Response::HTTP_NOT_FOUND);
            }

            $template = $this->container
                ->get('kadrige_api.template.handler')
                ->put($request->request->all(), $template);
            $this->container
                ->get('kadrige_api.repository.handler')
                ->publishCustom($template, $request->request->all());

            return $template;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }

    /**
     * upload image files
     * @View(statusCode=201, serializerGroups={"simple_template"})
     * @Put("/{id}/upload")
     *
     * @RequestParam(name="type", requirements="[a-zA-Z.-]+", description="image type", strict=true)
     *
     * @param Request $request
     * @param integer $id
     * @return object | Exception
     */
   public function uploadTemplateAction(Request $request, $id)
   {
       try {
         $template = $this->container
             ->get('kadrige_api.template.handler')
             ->get($id);

           if (!$template instanceof Template) {
              throw new HttpException('Template not found', Response::HTTP_NOT_FOUND);
           }

           $template = $this->container
               ->get('kadrige_api.template.handler')
               ->upload($request, $template, $this->container->getParameter("template.uploader.ext_allows"), $this->container->getParameter("template.uploader_name"));
           $this->container
               ->get('kadrige_api.repository.handler')
               ->saveCustom($template);

           return $template;

       } catch (HttpException $e) {
           return new errorResponse($e->getStatusCode(), $e->getMessage());
       }
   }


    /**
     * @View(statusCode=204, serializerGroups={"simple_template"})
     * @Delete("/{id}")
     *
     * @param integer $id
     * @return object | Exception
     */
    public function deleteTemplateAction($id)
    {
        try {
          $template = $this->container
              ->get('kadrige_api.template.handler')
              ->get($id);

            if (!$template instanceof Template) {
               throw new HttpException('Template not found or already deleted.', Response::HTTP_NOT_FOUND);
            }
            $this->container
                ->get('kadrige_api.repository.handler')
                ->removeCustom($template);

            $em = $this->getDoctrine()->getManager();
            $em->remove($template);
            $em->flush();

            return true;

        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }

    /**
     * @View(statusCode=204, serializerGroups={"simple_template"})
     * @Delete("")
     * @RequestParam(name="ids", requirements="[0-9,]+", description="template ids", strict=true)
     *
     * @param Request $request
     * @return object | Exception
     */
    public function deleteTemplatesAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();

            $aIds = explode(",",$request->request->get('ids'));

            foreach ($aIds as $id) {
                $this->deleteTemplateAction($id);
            }
            $em->getConnection()->commit();
            die();

        } catch (Exception $e) {
            $em->getConnection()->rollback();
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        } catch (HttpException $e) {
            $em->getConnection()->rollback();
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }
    }


}
