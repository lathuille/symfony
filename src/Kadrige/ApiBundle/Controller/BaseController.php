<?php

namespace Kadrige\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BaseController extends Controller
{

    /**
     * constructor
     */
	public function __construct()
	{
	}

    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('KadrigeApiBundle:Default:index.html.twig');
    }
}
