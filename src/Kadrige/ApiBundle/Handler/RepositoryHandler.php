<?php

namespace Kadrige\ApiBundle\Handler;

use Doctrine\ORM\EntityManager;
use Kadrige\ApiBundle\Entity\Template;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;
use Kadrige\ApiBundle\Component\HttpFoundation\errorResponse;

// -----
// TODO : use filesystem and Process classes instead of exec and file system commands (cp)

class RepositoryHandler implements RepositoryHandlerInterface
{
    protected $storageDir;
    protected $kernelRootDir;
    protected $fs;

  /**
   * @param RecursiveValidator $validator
   * @param string $KernelRootDir
   * @param string $storageDir
   * @return exception
   */
    public function __construct($kernelRootDir, $storageDir)
    {
        $this->kernelRootDir = $kernelRootDir;
        $this->storageDir = $this->kernelRootDir."/../src/Kadrige/ApiBundle/".$storageDir;
    }

    /**
    * create the template directory from the reference tempalte directory
    * and create a git branch
    * @param Template $template
    * @access public
    * @return void | exception
    */
     public function createCustom(Template $template)
     {
         $cmd = 'cd '.$this->storageDir." && cp -r ".$this->storageDir.'defaultnew-custom '.$template->getId().'-custom ';
         $cmd .= '&& cd '.$this->storageDir.'/'.$template->getId().'-custom ';
         $cmd .= '&& git checkout -b '.$template->getBranchName().' ';

         $result = exec($cmd);
     }

    /**
    * remove the template directory from the server and the bitbucket repository
    * @param Template $template
    * @access public
    * @return void | exception
    */
     public function removeCustom(Template $template)
     {
         $cmd = "cd ".$this->storageDir.$template->getId()."-custom && git checkout master && git branch -D ".$template->getBranchName();
         $cmd .= " && git push origin :".$template->getBranchName()." && cd ".$this->storageDir;
         $cmd .= ' && rm -rf '.$template->getId().'-custom';
echo $cmd;
         $result = exec($cmd);
     }

     /**
     * compile sass
     * @param Template $template
     * @access public
     * @return void | exception
     */
    public function compileCustom(Template $template)
    {
        $cmd = 'cd '.$this->storageDir.$template->getId().'-custom ';
        $cmd .= ' && sass --line-numbers --line-comments --force --update content/sass/main.scss:content/css/portail.css';
        $result1 = exec($cmd);

        $cmd = 'sass content/sass/main.scss:content/css/tools/portail.css';
        $result = exec($cmd);
    }

    /**
      * assets install
      * @param Template $template
      * @access public
      * @return void | exception
      */
     private function assetsInstallCustom(Template $template)
     {
         $cmd = 'cd '.$this->kernelRootDir.'../web';
         $cmd .= ' && php bin/console asssets:install';

         $result = exec($cmd);
     }

    /**
    * pushe custom repository on the remote branch
    * @param Template $template
    * @access public
    * @return void | exception
    */
    public function releaseCustom(Template $template)
    {
        $cmd = 'cd '.$this->storageDir.$template->getId().'-custom ';
        $cmd .= '&& git add --all  && git commit -m "Add repo "'.$template->getBranchName().' && git push origin '.$template->getBranchName().' ';

        $result = exec($cmd,$opt);
    }

    /**
    * complie le sass et pushe la custom sur le repo distant
    * @param Template $template
    * @access public
    * @return void | exception
    */
    public function saveCustom(Template $template)
    {
         $this->compileCustom($template);
         $this->assetsInstallCustom($template);
         $this->releaseCustom($template);
    }

    /**
    * call kid remote platform ws in order to pull the client custom branch
    * @param Template $template
    * @access public
    * @return void | exception
    */
    public function publishCustom(Template $template, $aParameters)
    {
        try {
            $this->saveCustom($template);

            $aPlateforme = (null !== $aParameters['plateforme']) ? $aParameters['plateforme'] : "";
            if (is_array($aPlateforme) && count($aPlateforme)>0)
            {
                foreach ($aPlateforme as $key => $value)
                {
                    if ( empty($value['plateforme_name']) || !filter_var(trim($value['plateforme_name']), FILTER_VALIDATE_URL)) {
                        throw new HttpException("the remote platform url is invalid", Response::HTTP_BAD_REQUEST);
                    }
                    $this->callRemotePlatform($template, $value['plateforme_name']);
                }
            }

        } catch (CurlException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        } catch (HttpException $e) {
            return new errorResponse($e->getStatusCode(), $e->getMessage());
        }

    }

    /**
     * appelle par curl le script de la plateforme distante
     * @param Template $template
     * @param array $aParameters
     * @access public
     * @return void
     */
    public static function callRemotePlatform(Template $template, $sPlateformeRemote)
    {
        $curl = curl_init();

        $sPlateformeRemote = (strpos(trim($sPlateformeRemote),"https://")) === false ? 'https://'.$sPlateformeRemote : $sPlateformeRemote;
        curl_setopt($curl, CURLOPT_URL,$sPlateformeRemote.'/api/tuning.php');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,  false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $sData = http_build_query(array ('branchname' => $template->getBranchname(),'type_mapping' => 'publishtemplate', 'src' => $_SERVER['HTTP_HOST']));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $sData);

        $result = curl_exec($curl);
        if ($result === false) {
            throw new CurlException("Error when trying to connect to ".$sPlateformeRemote.'/api/tuning.php'." : ".curl_error($curl));
        }

        curl_close($curl);
        return $result;
    }


}
