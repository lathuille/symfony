<?php

namespace Kadrige\ApiBundle\Handler;

use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileHandlerInterface {

  public function __construct(RecursiveValidator $validator, $KernelRootDir, $storageDir);
  public function saveFile($sPathFileToSave, $sContentFile);
  public function moveFile(UploadedFile $file, $template, $type, $aTypeNames);
}
