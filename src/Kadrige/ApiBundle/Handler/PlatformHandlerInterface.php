<?php

namespace Kadrige\ApiBundle\Handler;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Kadrige\ApiBundle\Entity\Platform;

interface PlatformHandlerInterface {

  public function __construct(EntityManager $em, RecursiveValidator $validator, FileHandlerInterface $fileHandler, RepositoryHandlerInterface $repositoryHandler, $entityClass);
  public function get($id);
  public function findBy($aParameters);
  public function validateParameters($aParameters);
  public function check($aParameters);

  public function post($aParameters, Platform $platform);
  public function put($aParameters, Platform $platform);
  public function processPlatform($aParameters, Platform $platform);
}
