<?php

namespace Kadrige\ApiBundle\Handler;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Kadrige\ApiBundle\Entity\Template;

interface TemplateHandlerInterface {

  public function __construct(EntityManager $em, RecursiveValidator $validator, FileHandlerInterface $fileHandler, RepositoryHandlerInterface $repositoryHandler, $entityClass);
  public function get($id);
  public function findBy($aParameters);
  public function validateParameters($aParameters);
  public function post($aParameters, Template $template);
  public function put($aParameters, Template $template);
  public function delete(Template $template);
  public function upload($aParameters, Template $template, $aExtAllowed, $aTypeNames);

  public function processUploadFilesTemplate($request, Template $template, $aExtAllowed, $aTypeNames);
  public function processTemplate($aParameters, Template $template);
  public function processFilesTemplate($aParameters, Template $template);
}
