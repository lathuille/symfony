<?php

namespace Kadrige\ApiBundle\Handler;

use Doctrine\ORM\EntityManager;
use Kadrige\ApiBundle\Entity\TemplatePlatform;
use Kadrige\ApiBundle\Entity\Template;
use Kadrige\ApiBundle\Entity\Platform;

use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class TemplatePlatformHandler implements TemplatePlatformHandlerInterface
{
    protected $validator;
    protected $em;
    protected $entityClass;
    protected $repository;
    protected $fileHandler;
    protected $repositoryHandler;
    protected $templateUploadAllowed;
    protected $templateUploaderExtAallows;

    protected $_aFileUploadErrors = array(
       UPLOAD_ERR_OK => 'There is no error, the file uploaded with success',
       UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
       UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
       UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded',
       UPLOAD_ERR_NO_FILE => 'No file was uploaded',
       UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder',
       UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
       UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload.',
   );

  /**
   * @param EntityManager $em
   * @param RecursiveValidator $validator
   * @param FileHandlerInterface $FileHandler
   * @param $entityClass
   * @return
   */
    public function __construct(EntityManager $em, RecursiveValidator $validator, FileHandlerInterface $fileHandler, RepositoryHandlerInterface $repositoryHandler, $entityClass)
    {
        $this->em = $em;
        $this->entityClass = $entityClass;
        $this->repository = $this->em->getRepository($this->entityClass);
        $this->validator = $validator;
        $this->fileHandler = $fileHandler;
        $this->repositoryHandler = $repositoryHandler;
    }

    /**
     * @param integer $id
     * @return Template Object
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $arraySearch
     * @param array $arraySort
     * @return array collection
     */
    public function findBy($aParameters)
    {
        $arraySearch = array();
        $arraySort = array();

        $this->validateParameters($aParameters);

        $id_user = isset($aParameters['user_id']) ? $aParameters['user_id'] : "";
        $client = isset($aParameters['client']) ? $aParameters['client'] : "";
        $id_plateforme = isset($aParameters['plateforme_id']) ? $aParameters['plateforme_id'] : "";

        $sort = isset($aParameters['sort']) ? $aParameters['sort'] : "";
        $sortorder = isset($aParameters['sortorder']) ? $aParameters['sortorder'] : "";

        if (isset($client) && !empty($client)) {
            $arraySearch['client'] = $client;
        }
        if (isset($id_plateforme) && !empty($id_plateforme)) {
            $arraySearch['plateformeId'] = $id_plateforme;
        }
        if (isset($id_user) && !empty($id_user)) {
            $arraySearch['userId'] = $id_user;
        }
        if (isset($sort) && !empty($sort) ) {
            $arraySort[$sort] = (isset($sortorder) && !empty($sortorder) ) ? $sortorder : "desc";
        }

        return $this->repository->findByCriteria($arraySearch, $arraySort);
    }

    /**
     * @param array $aParameters
     * @return exception
     */
    public function validateParameters($aParameters)
    {
        $aErrors = array();
        $constraintsClient = array(
           new \Symfony\Component\Validator\Constraints\NotNull(),
           new \Symfony\Component\Validator\Constraints\NotBlank()
        );
        $constraintsId = array(
           new \Symfony\Component\Validator\Constraints\Regex("/^\d+$/"),
        );
        $constraintsEmail = array(
           new \Symfony\Component\Validator\Constraints\Email(),
        );

        if (isset($aParameters['client']) && !empty($aParameters['client'])) {
            $error = $this->validator->validate($aParameters['client'], $constraintsClient);

            if (count($error) > 0) {
                $aErrors[] = $error;
            }
        }
        if (isset($aParameters['plateforme_id']) && !empty($aParameters['plateforme_id'])) {
            $error = $this->validator->validate($aParameters['plateforme_id'], $constraintsId);

            if (count($error) > 0) {
                $aErrors[] = $error;
            }
        }
        if (isset($aParameters['user_id']) && !empty($aParameters['user_id'])) {
          $error = $this->validator->validate($aParameters['user_id'], $constraintsEmail);

          if (count($error) > 0) {
              $aErrors[] = $error;
          }
        }

        if (count($aErrors) > 0) {
            $errorsString = implode(",", $aErrors);
            throw new HttpException($errorsString, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * checks if a platform already exists, returns if so else creates the platform
     *
     * @param array $aParameters
     * @param Platform $platform
     * @param boolean $modfication
     * @return Platform | exceptiondisclaimer
     */
    public function check($aParameters, Template $template, Platform $platform, TemplatePlatform $templatePlatform)
    {
        $constraintsId = array(
           new \Symfony\Component\Validator\Constraints\Regex("/^\d+$/"),
        );
        if (isset($aParameters['plateforme_id']) && !empty($aParameters['plateforme_id'])) {
            $error = $this->validator->validate($aParameters['plateforme_id'], $constraintsId);

            if (count($error) > 0) {
                $aErrors[] = $error;
            }
        }
        $aTemplatePlatforms = $this->repository->findBy(array(
            'template' => $template,
            'plateforme' => $platform
        ));
        $templatePlatform = (count($aTemplatePlatforms) == 0) ? $this->post($aParameters, $template, $platform, $templatePlatform)
            : false;

        return $templatePlatform;
    }

    /**
     * @param array $aParameters
     * @param TemplatePlatform $templatePlatform
     * @return TemplatePlatform | exceptiondisclaimer
     */
    public function post($aParameters, Template $template, Platform $platform, TemplatePlatform $templatePlatform)
    {
        $sError = "";
        $templatePlatform = $this->processTemplatePlatform($aParameters, $template, $platform, $templatePlatform);
        $errors = $this->validator->validate($templatePlatform, null, array('registration'));

        if (isset($errors) && $errors->count() > 0) {
            foreach ($errors as $error) {
                $sError .= $error->getMessage().". ";
            }
            throw new HttpException($sError, Response::HTTP_BAD_REQUEST);
        }
        $this->em->persist($templatePlatform);
        $this->em->flush();

        return $templatePlatform;
    }

    /**
     * @param array $aParameters
     * @param TemplatePlatform $templatePlatform
     * @return TemplatePlatform | exceptiondisclaimer
     */
    public function put($aParameters, Template $template, Platform $platform, TemplatePlatform $templatePlatform)
    {
        $sError = "";
        $template = $this->processTemplatePlatform($aParameters, $template, $platform, $templatePlatform);
        $errors = $this->validator->validate($platform, null, array('registration'));

        if (isset($errors) && $errors->count() > 0) {
            foreach ($errors as $error) {
                $sError .= $error->getMessage().". ";
            }
            throw new HttpException($sError, Response::HTTP_BAD_REQUEST);
        }
        $this->em->flush();
        return $templatePlatform;
    }

    /**
     * @param array $aParameters
     * @param Platform $platform
     * @return Platform | exceptiondisclaimer
     */
    public function processTemplatePlatform($aParameters, Template $template, Platform $platform, TemplatePlatform $templatePlatform)
    {
        $userId = isset($aParameters['user_id']) ? $aParameters['user_id'] : "";

        if (isset ($userId)) {
            $templatePlatform->setUserId($userId);
        }
        if ($template instanceof Template) {
            $templatePlatform->setTpl($template);
        }
        if ($platform instanceof Platform) {
            $templatePlatform->setPlateforme($platform);
        }

        return $templatePlatform;
    }

}
