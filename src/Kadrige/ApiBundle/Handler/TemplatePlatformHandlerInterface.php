<?php

namespace Kadrige\ApiBundle\Handler;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator\RecursiveValidator;

use Kadrige\ApiBundle\Entity\TemplatePlatform;
use Kadrige\ApiBundle\Entity\Template;
use Kadrige\ApiBundle\Entity\Platform;

interface TemplatePlatformHandlerInterface {

    public function __construct(EntityManager $em, RecursiveValidator $validator, FileHandlerInterface $fileHandler, RepositoryHandlerInterface $repositoryHandler, $entityClass);
    public function get($id);
    public function findBy($aParameters);
    public function validateParameters($aParameters);
    public function check($aParameters, Template $template, Platform $platform, TemplatePlatform $templatePlatform);

    public function post($aParameters, Template $template, Platform $platform, TemplatePlatform $templatePlatform);
    public function put($aParameters, Template $template, Platform $platform, TemplatePlatform $templatePlatform);

    public function processTemplatePlatform($aParameters, Template $template, Platform $platform, TemplatePlatform $templatePlatform);
}
