<?php

namespace Kadrige\ApiBundle\Handler;

use Doctrine\ORM\EntityManager;
use Kadrige\ApiBundle\Entity\Platform;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class PlatformHandler implements PlatformHandlerInterface
{
    protected $validator;
    protected $em;
    protected $entityClass;
    protected $repository;
    protected $fileHandler;
    protected $repositoryHandler;
    protected $templateUploadAllowed;
    protected $templateUploaderExtAallows;

    protected $_aFileUploadErrors = array(
       UPLOAD_ERR_OK => 'There is no error, the file uploaded with success',
       UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
       UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
       UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded',
       UPLOAD_ERR_NO_FILE => 'No file was uploaded',
       UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder',
       UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
       UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload.',
   );

  /**
   * @param EntityManager $em
   * @param RecursiveValidator $validator
   * @param FileHandlerInterface $FileHandler
   * @param $entityClass
   * @return
   */
    public function __construct(EntityManager $em, RecursiveValidator $validator, FileHandlerInterface $fileHandler, RepositoryHandlerInterface $repositoryHandler, $entityClass)
    {
        $this->em = $em;
        $this->entityClass = $entityClass;
        $this->repository = $this->em->getRepository($this->entityClass);
        $this->validator = $validator;
        $this->fileHandler = $fileHandler;
        $this->repositoryHandler = $repositoryHandler;
    }

    /**
     * @param integer $id
     * @return Template Object
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $arraySearch
     * @param array $arraySort
     * @return array collection
     */
    public function findBy($aParameters)
    {
        $arraySearch = array();
        $arraySort = array();

        $this->validateParameters($aParameters);

        $clientName = isset($aParameters['client']) ? $aParameters['client'] : "";
        $id_plateforme = isset($aParameters['plateforme_id']) ? $aParameters['plateforme_id'] : "";

        $sort = isset($aParameters['sort']) ? $aParameters['sort'] : "";
        $sortorder = isset($aParameters['sortorder']) ? $aParameters['sortorder'] : "";

        if (isset($clientName) && !empty($clientName)) {
            $arraySearch['clientName'] = $clientName;
        }
        if (isset($id_plateforme) && !empty($id_plateforme)) {
            $arraySearch['plateforme_id'] = $id_plateforme;
        }
        if (isset($sort) && !empty($sort) ) {
            $arraySort[$sort] = (isset($sortorder) && !empty($sortorder) ) ? $sortorder : "desc";
        }

        return $this->repository->findByCriteria($arraySearch, $arraySort);
    }

    /**
     * @param array $aParameters
     * @return exception
     */
    public function validateParameters($aParameters)
    {
        $aErrors = array();
        $constraintsClient = array(
           new \Symfony\Component\Validator\Constraints\NotNull(),
           new \Symfony\Component\Validator\Constraints\NotBlank()
        );
        $constraintsId = array(
           new \Symfony\Component\Validator\Constraints\Regex("/^\d+$/"),
        );

        if (isset($aParameters['client']) && !empty($aParameters['client'])) {
            $error = $this->validator->validate($aParameters['client'], $constraintsClient);

            if (count($error) > 0) {
                $aErrors[] = $error;
            }
        }
        if (isset($aParameters['plateforme_id']) && !empty($aParameters['plateforme_id'])) {
            $error = $this->validator->validate($aParameters['plateforme_id'], $constraintsId);

            if (count($error) > 0) {
                $aErrors[] = $error;
            }
        }
        if (isset($aParameters['plateforme_name']) && !empty($aParameters['plateforme_name'])) {
            $error = $this->validator->validate($aParameters['plateforme_name'], $constraintsId);

            if (count($error) > 0) {
                $aErrors[] = $error;
            }
        }

        if (count($aErrors) > 0) {
            $errorsString = implode(",", $aErrors);
            throw new HttpException($errorsString, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * checks if a platform already exists, returns if so else creates the platform
     *
     * @param array $aParameters
     * @return Platform | exceptiondisclaimer
     */
    public function check($aParameters)
    {
        $constraintsId = array(
           new \Symfony\Component\Validator\Constraints\Regex("/^\d+$/"),
        );
        if (isset($aParameters['plateforme_id']) && !empty($aParameters['plateforme_id'])) {
            $error = $this->validator->validate($aParameters['plateforme_id'], $constraintsId);

            if (count($error) > 0) {
                $aErrors[] = $error;
            }
        }
        $platform = $this->repository->findOneBy(array('mpaId' => $aParameters['plateforme_id']));
        if ($platform instanceof Platform) {
           return $platform;
        }

        $sError = "";
        $platform = $this->processPlatform($aParameters,new Platform());
        $errors = $this->validator->validate($platform, null, array('registration'));

        if (isset($errors) && $errors->count() > 0) {
            foreach ($errors as $error) {
                $sError .= $error->getMessage().". ";
            }
            throw new HttpException($sError, Response::HTTP_BAD_REQUEST);
        }
        $this->em->persist($platform);
        $this->em->flush();

        return $platform;
    }


    /**
     * @param array $aParameters
     * @param Platform $platform
     * @return Platform | exceptiondisclaimer
     */
    public function post($aParameters, Platform $platform)
    {
        $sError = "";
        $platform = $this->processPlatform($aParameters,$platform);
        $errors = $this->validator->validate($platform, null, array('registration'));

        if (isset($errors) && $errors->count() > 0) {
            foreach ($errors as $error) {
                $sError .= $error->getMessage().". ";
            }
            throw new HttpException($sError, Response::HTTP_BAD_REQUEST);
        }
        $this->em->persist($platform);
        $this->em->flush();

        return $platform;
    }

    /**
     * @param array $aParameters
     * @param Platform $platform
     * @return Platform | exceptiondisclaimer
     */
    public function put($aParameters, Platform $platform)
    {
        $sError = "";
        $platform = $this->processPlatform($aParameters, $platform);
        $errors = $this->validator->validate($platform, null, array('registration'));

        if (isset($errors) && $errors->count() > 0) {
            foreach ($errors as $error) {
                $sError .= $error->getMessage().". ";
            }
            throw new HttpException($sError, Response::HTTP_BAD_REQUEST);
        }
        $this->em->flush();
        return $platform;
    }

    /**
     * @param array $aParameters
     * @param Platform $platform
     * @return Platform | exceptiondisclaimer
     */
    public function processPlatform($aParameters, Platform $platform)
    {
        $client = isset($aParameters['client']) ? $aParameters['client'] : "";
        $idPlateforme = isset($aParameters['plateforme_id']) ? $aParameters['plateforme_id'] : "";
        $namePlateforme = isset($aParameters['plateforme_name']) ? $aParameters['plateforme_name'] : "";

        if (isset ($client)) {
            $platform->setClientName($client);
        }
        if (isset ($idPlateforme)) {
            $platform->setMpaId($idPlateforme);
        }
        if (isset ($namePlateforme)) {
            $platform->setName($namePlateforme);
        }

        return $platform;
    }

}
