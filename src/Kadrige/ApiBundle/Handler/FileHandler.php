<?php

namespace Kadrige\ApiBundle\Handler;

use Doctrine\ORM\EntityManager;
use Kadrige\ApiBundle\Entity\Template;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileHandler implements FileHandlerInterface
{
    protected $validator;
    protected $kernelRootDir;
    protected $storageDir;
    protected $fs;

  /**
   * @param RecursiveValidator $validator
   * @param string $KernelRootDir
   * @param string $storageDir
   */
    public function __construct(RecursiveValidator $validator, $kernelRootDir, $storageDir)
    {
        $this->fs = new Filesystem();

        $this->validator = $validator;
        $this->kernelRootDir = $kernelRootDir;
        $this->storageDir = $this->kernelRootDir."/../src/Kadrige/ApiBundle/".$storageDir;
    }

    /**
     * @param string $sPathFileToSave
     * @param string $sContentFile
     * @return void | exception
     */
    public function saveFile($sPathFileToSave, $sContentFile)
    {
        try {
            $this->fs->dumpFile($this->storageDir.$sPathFileToSave, $sContentFile);

        } catch (IOException $e) {
            throw new HttpException($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param UploadedFile $file
     * @param Template
     * @param string $type
     * @param array $aTypeNames
     * @return void | exception
     */
    public function moveFile(UploadedFile $file, $template, $type, $aTypeNames)
    {
        $sPathFileToSave = $template->getId()."-custom".$aTypeNames['path'];
        $file->move($this->storageDir.$sPathFileToSave, $aTypeNames[$type]);

        if (strcmp(mb_strtolower(trim($type)), "logo") == 0)
        {
          $cmd = 'cd '.$this->storageDir.$sPathFileToSave." && cp logo.jpg logo.png";
          $result = exec($cmd);
        }
    }



}
