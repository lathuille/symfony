<?php

namespace Kadrige\ApiBundle\Handler;

use Doctrine\ORM\EntityManager;
use Kadrige\ApiBundle\Entity\Template;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class TemplateHandler implements TemplateHandlerInterface
{
    protected $validator;
    protected $em;
    protected $entityClass;
    protected $repository;
    protected $fileHandler;
    protected $repositoryHandler;
    protected $templateUploadAllowed;
    protected $templateUploaderExtAallows;

    protected $_aFileUploadErrors = array(
       UPLOAD_ERR_OK => 'There is no error, the file uploaded with success',
       UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
       UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
       UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded',
       UPLOAD_ERR_NO_FILE => 'No file was uploaded',
       UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder',
       UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
       UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload.',
   );

  /**
   * @param EntityManager $em
   * @param RecursiveValidator $validator
   * @param FileHandlerInterface $FileHandler
   * @param $entityClass
   * @return
   */
    public function __construct(EntityManager $em, RecursiveValidator $validator, FileHandlerInterface $fileHandler, RepositoryHandlerInterface $repositoryHandler, $entityClass)
    {
        $this->em = $em;
        $this->entityClass = $entityClass;
        $this->repository = $this->em->getRepository($this->entityClass);
        $this->validator = $validator;
        $this->fileHandler = $fileHandler;
        $this->repositoryHandler = $repositoryHandler;
    }

    /**
     * @param integer $id
     * @return Template Object
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $arraySearch
     * @param array $arraySort
     * @return array collection
     */
    public function findBy($aParameters)
    {
        $arraySearch = array();
        $arraySort = array();

        $this->validateParameters($aParameters);

        $id_user = isset($aParameters['user_id']) ? $aParameters['user_id'] : "";
        $client = isset($aParameters['client']) ? $aParameters['client'] : "";
        $id_plateforme = isset($aParameters['plateforme_id']) ? $aParameters['plateforme_id'] : "";

        $sort = isset($aParameters['sort']) ? $aParameters['sort'] : "";
        $sortorder = isset($aParameters['sortorder']) ? $aParameters['sortorder'] : "";

        if (isset($client) && !empty($client)) {
            $arraySearch['client'] = $client;
        }
        if (isset($id_plateforme) && !empty($id_plateforme)) {
            $arraySearch['plateformeId'] = $id_plateforme;
        }
        if (isset($id_user) && !empty($id_user)) {
            $arraySearch['userId'] = $id_user;
        }
        if (isset($sort) && !empty($sort) ) {
            $arraySort[$sort] = (isset($sortorder) && !empty($sortorder) ) ? $sortorder : "desc";
        }

        return $this->repository->findByCriteria($arraySearch, $arraySort);
    }

    /**
     * @param array $aParameters
     * @return exception
     */
    public function validateParameters($aParameters)
    {
        $aErrors = array();
        $constraintsClient = array(
           new \Symfony\Component\Validator\Constraints\NotNull(),
           new \Symfony\Component\Validator\Constraints\NotBlank()
        );
        $constraintsId = array(
           new \Symfony\Component\Validator\Constraints\Regex("/^\d+$/"),
        );
        $constraintsEmail = array(
           new \Symfony\Component\Validator\Constraints\Email(),
        );

        if (isset($aParameters['client']) && !empty($aParameters['client'])) {
            $error = $this->validator->validate($aParameters['client'], $constraintsClient);

            if (count($error) > 0) {
                $aErrors[] = $error;
            }
        }
        if (isset($aParameters['plateforme_id']) && !empty($aParameters['plateforme_id'])) {
            $error = $this->validator->validate($aParameters['plateforme_id'], $constraintsId);

            if (count($error) > 0) {
                $aErrors[] = $error;
            }
        }
        if (isset($aParameters['user_id']) && !empty($aParameters['user_id'])) {
          $error = $this->validator->validate($aParameters['user_id'], $constraintsEmail);

          if (count($error) > 0) {
              $aErrors[] = $error;
          }
        }

        if (count($aErrors) > 0) {
            $errorsString = implode(",", $aErrors);
            throw new HttpException($errorsString, Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param array $aParameters
     * @param Template $template
     * @return Template | exception
     */
    public function post($aParameters, Template $template)
    {
        $sError = "";
        $template = $this->processTemplate($aParameters,$template);
        $errors = $this->validator->validate($template, null, array('registration'));

        if (isset($errors) && $errors->count() > 0) {
            foreach ($errors as $error) {
                $sError .= $error->getMessage().". ";
            }
            throw new HttpException($sError, Response::HTTP_BAD_REQUEST);
        }
        $branchName = "custo-".uniqid();
        $template->setName($branchName);
        $template->setBranchName($branchName);
        $this->em->persist($template);
        $this->em->flush();

        $this->repositoryHandler->createCustom($template);
        $this->processFilesTemplate($aParameters, $template);

        return $template;
    }

    /**
     * @param Template $template
     * @return void | exception
     */
    public function put($aParameters, Template $template)
    {
        $sError = "";
        $template = $this->processTemplate($aParameters, $template);
        $errors = $this->validator->validate($template, null, array('registration'));

        if (isset($errors) && $errors->count() > 0) {
            foreach ($errors as $error) {
                $sError .= $error->getMessage().". ";
            }
            throw new HttpException($sError, Response::HTTP_BAD_REQUEST);
        }
        $this->processFilesTemplate($aParameters, $template);

        $this->em->flush();
        return $template;
    }

    /**
     * @param Template $template
     * @return Template | exception
     */
    public function delete(Template $template)
    {
        $sError = "";
        $aTemplatePlatforms = $template->getTemplatePlatforms();

        if (count($aTemplatePlatforms)>0) {
            foreach ($aTemplatePlatforms as $key => $tplPlatform) {
                echo 'tpl='.$tplPlatform->getPlateforme()->getName()."<br/>";
                $this->em->remove($tplPlatform);
            }
        }
        $this->repositoryHandler->removeCustom($template);

        $this->em->remove($template);
        $this->em->flush();

        return true;
    }


    /**
     * @param array $aParameters
     * @param Template $template
     * @param array $aExtAllowed
     * @param array $aTypeNames
     * @return Template | exception
     */
    public function upload($request, Template $template, $aExtAllowed, $aTypeNames)
    {
        $this->processUploadFilesTemplate($request, $template, $aExtAllowed, $aTypeNames);

        $this->em->flush();
        return $template;
    }

    /**
     * @param array $aParameters
     * @param Template $template
     * @param array $aExtAllowed
     * @param array $aTypeNames
     * @return void | exception
     */
    public function processUploadFilesTemplate($request, Template $template, $aExtAllowed, $aTypeNames)
    {
        $type = (null !== $request->request->get('type')) ? $request->request->get('type') : "";
        $file = $request->files->get('file');

        if ($file == null) {
            throw new HttpException("The file is required", Response::HTTP_BAD_REQUEST);
        }
        if ($file->isValid() != true) {
            throw new HttpException($this->_aFileUpload[$file->getError()], Response::HTTP_BAD_REQUEST);
        }
        if (!array_key_exists(mb_strtolower(trim($type)), $aTypeNames)) {
            throw new HttpException("The type associated with the image is not allowed.", Response::HTTP_BAD_REQUEST);
        }
        if (!in_array(mb_strtolower($file->getClientOriginalExtension()), $aExtAllowed)) {
            throw new HttpException("File extension not valid.", Response::HTTP_BAD_REQUEST);
        }

        $this->fileHandler->moveFile($file, $template, $type, $aTypeNames);
    }

    /**
     * @param array $aParameters
     * @param Template $template
     * @return Template | exception
     */
    public function processTemplate($aParameters, Template $template)
    {
        $disclaimer = isset($aParameters['disclaimer']) ? $aParameters['disclaimer'] : "";
        $adresse = isset($aParameters['adresse']) ? $aParameters['adresse'] : "";
        $url = isset($aParameters['url']) ? $aParameters['url'] : "";
        $color = isset($aParameters['color']) ? $aParameters['color'] : "";
        $colorb = isset($aParameters['colorb']) ? $aParameters['colorb'] : "";
        $lang = isset($aParameters['lang']) ? $aParameters['lang'] : "";

        if (isset ($disclaimer)) {
            $template->setDisclaimer(json_encode($disclaimer));
        }
        if (isset ($adresse)) {
            $template->setAdresse($adresse);
        }
        if (isset ($url)) {
            $template->setUrl($url);
        }
        if (isset ($color) && isset ($colorb)) {
            $template->setColor($color);
            $template->setColorb($colorb);

            $globalScss = chr(36)."font-color-b : ".$colorb."; ".chr(36)."font-color : ".$color.";";
        }
        $template->setDateCreation(new \DateTime());

        return $template;
    }

    /**
     * @param array $aParameters
     * @param Template $template
     * @return exception
     */
    public function processFilesTemplate($aParameters, Template $template)
    {
        $disclaimer = isset($aParameters['disclaimer']) ? $aParameters['disclaimer'] : "";
        $adresse = isset($aParameters['adresse']) ? $aParameters['adresse'] : "";
        $url = isset($aParameters['url']) ? $aParameters['url'] : "";
        $color = isset($aParameters['color']) ? $aParameters['color'] : "";
        $colorb = isset($aParameters['colorb']) ? $aParameters['colorb'] : "";

        if (isset ($disclaimer)) {
            $aDisclaimers = $disclaimer;
	    if (is_array($aDisclaimers) && count($aDisclaimers)>0) {
                foreach ($aDisclaimers as $aDisclaimer)
                {
                    $disclaimer = '#. DISCLAIMER'.chr(10).'msgid "DISCLAIMER"'.chr(10).'msgstr "'.$aDisclaimer['content'].'"';
                    $this->fileHandler->saveFile($template->getId().'-custom/languages/'.$aDisclaimer["prefix"].'.po', $disclaimer);
                }
            } else  {
	        $disclaimer = '#. DISCLAIMER'.chr(10).'msgid "DISCLAIMER"'.chr(10).'msgstr "'.$disclaimer.'"';
                $this->fileHandler->saveFile($template->getId().'-custom/languages/en_GB.po', $disclaimer);
	    }
        }

        if (isset ($adresse)) {
            $this->fileHandler->saveFile($template->getId().'-custom/templates/adresse.tpl', $adresse);
        }
        if (isset ($url)) {
            $this->fileHandler->saveFile($template->getId().'-custom/templates/url.tpl', $url);
        }
        if (isset ($color) && isset ($colorb)) {
            $globalScss = chr(36)."font-color-b : ".$colorb."; ".chr(36)."font-color : ".$color.";";
            $this->fileHandler->saveFile($template->getId().'-custom/content/sass/_global.scss', $globalScss);
        }
    }


}
