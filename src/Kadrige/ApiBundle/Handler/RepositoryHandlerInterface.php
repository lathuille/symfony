<?php

namespace Kadrige\ApiBundle\Handler;

use Symfony\Component\Validator\Validator\RecursiveValidator;
use Kadrige\ApiBundle\Entity\Template;

interface RepositoryHandlerInterface {

  public function __construct($KernelRootDir, $storageDir);
  public function createCustom(Template $template);
  public function removeCustom(Template $template);
  public function compileCustom(Template $template);
  private function assetsInstallCustom(Template $template);

  public function releaseCustom(Template $template);
  public function saveCustom(Template $template);
  public function publishCustom(Template $template, $aParameters);
  public static function callRemotePlatform(Template $template, $aParameters);
}
