<?php

namespace Kadrige\ApiBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Kadrige\ApiBundle\Entity\Template;

class LoadTestData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $aTestData = array(
            array( "defineName" => "__MaxUsers", "value" => "3",  "commentaire" => 'Max users number allowed on the platform', "type" => "3", "categorie" => "2", "importance" => "1"),
            array( "defineName" => "__sInternalKey", "value" => "Cle_visitecrm_1928374",  "commentaire" => 'visit key platform', "type" => "3", "categorie" => "2", "importance" => "1"),
            array( "defineName" => "__bHideCRMButton", "value" => "false",  "commentaire" => 'Disable crm functionnality (true)', "type" => "3", "categorie" => "2", "importance" => "1"),
            array( "defineName" => "__bAutoDetectLanguage", "value" => "1",  "commentaire" => 'Browser locale autodetection', "type" => "3", "categorie" => "2", "importance" => "1"),
            array( "defineName" => "S_AVAILABLE_LOCALE", "value" => "a:16:{i:0;s:5:'fr_FR';i:1;s:5:'es_ES';i:2;s:5:'tr_TR';i:3;s:5:'ru_RU';i:4;s:5:'pt_PT';i:5;s:5:'pt_BR';i:6;s:5:'pl_PL';i:7;s:5:'no_NO';i:8;s:5:'it_IT';i:9;s:5:'gr_GR';i:10;s:5:'en_GB';i:11;s:5:'en_US';i:12;s:5:'de_DE';i:13;s:5:'cn_CN';i:14;s:5:'es_PE';i:15;s:5:'nl_NL';}",  "commentaire" => 'platform languages', "type" => "3", "categorie" => "2", "importance" => "1")
        );

        foreach ($aTestData as $aValue)
        {
            $settingAdmin = new Setting();
            $settingAdmin->setDefineName($aValue['defineName']);
            $settingAdmin->setValue($aValue['value']);
            $settingAdmin->setCommentaire($aValue['commentaire']);
            $settingAdmin->setType($aValue['type']);
            $settingAdmin->setCategorie($aValue['categorie']);
            $settingAdmin->setImportance($aValue['importance']);

            $manager->persist($settingAdmin);
            $manager->flush();
        }
    }
}
