<?php

namespace Kadrige\ApiBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Kadrige\ApiBundle\Entity\Template;

class LoadTemplateData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $disclaimer = '[{"prefix":"en_US","content":"<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer<\/p>"},
            {"prefix":"fr_FR","content":"<p class=\'disclaimer_right\'>disclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimerdisclaimer<\/p>"}]';

        $aTestData = array(
            array( "name" => "custo-5729c09be9743", "branchName" => "custo-5729c09be9743",  "disclaimer" => $disclaimer, "adresse" => "adresse adresse adresse ", "url" => "http://www.kadrige.com"),
            array( "name" => "custo-5729c09be9744", "branchName" => "custo-5729c09be9744",  "disclaimer" => $disclaimer, "adresse" => "adresse adresse adresse ", "url" => "http://www.kadrige.com"),
            array( "name" => "custo-5729c09be9745", "branchName" => "custo-5729c09be9745",  "disclaimer" => $disclaimer, "adresse" => "adresse adresse adresse ", "url" => "http://www.kadrige.com"),
        );

        foreach ($aTestData as $aValue)
        {
            $template = new Template();

            $template->setName($aValue['name']);
            $template->setBranchName($aValue['branchName']);
            $template->setDisclaimer($aValue['disclaimer']);
            $template->setAdresse($aValue['adresse']);
            $template->setUrl($aValue['url']);
            $template->setColorb('');
            $template->setColor('');

            $manager->persist($template);
            $manager->flush();

            $this->addReference('template-'.$aValue['name'], $template);
        }

    }

    /**
     * @return void
     */
    public function getOrder()
    {
        return 2;
    }
}
