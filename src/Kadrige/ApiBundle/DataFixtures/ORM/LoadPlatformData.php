<?php

namespace Kadrige\ApiBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Kadrige\ApiBundle\Entity\Platform;

class LoadPlatformData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $aTestData = array(
            array( "name" => "https://appremote-dev.qa.keo.net/", "clientName" => "AMGEN",  "mpaId" => "155"),
            array( "name" => "https://appremote2-dev.qa.keo.net/", "clientName" => "AMGEN",  "mpaId" => "32"),
            array( "name" => "https://appremote3-dev.qa.keo.net/", "clientName" => "BMS",  "mpaId" => "124"),
            array( "name" => "https://appremote4-dev.qa.keo.net/", "clientName" => "BMS",  "mpaId" => "125"),
            array( "name" => "https://appremote5-dev.qa.keo.net/", "clientName" => "TAKEDA",  "mpaId" => "127")
        );

        foreach ($aTestData as $aValue)
        {
            $platform = new Platform();

            $platform->setName($aValue['name']);
            $platform->setClientName($aValue['clientName']);
            $platform->setMpaId($aValue['mpaId']);

            $manager->persist($platform);
            $manager->flush();

            $this->addReference('platform-'.$aValue['mpaId'], $platform);
        }

    }

    /**
     * @return void
     */
    public function getOrder()
    {
        return 1;
    }
}
