<?php

namespace Kadrige\ApiBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Kadrige\ApiBundle\Entity\TemplatePlatform;

class LoadTemplatePlatformData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $aTpl = array("template-custo-5729c09be9743", "template-custo-5729c09be9744", "template-custo-5729c09be9745");
        $aPlf = array("platform-124", "platform-125", "platform-127");

        foreach ($aTpl as $key => $value)
        {
            foreach ($aPlf as $key => $valuePlf)
            {
                $tplPlatform = new TemplatePlatform();

                $tplPlatform->setUserId("raymond@kadrige.com");
                $tplPlatform->setTpl($this->getReference($value));
                $tplPlatform->setPlateforme($this->getReference($valuePlf));

                $manager->persist($tplPlatform);
            }

        }
        $manager->flush();
    }

    /**
     * @return void
     */
    public function getOrder()
    {
        return 3;
    }
}
