<?php

namespace Kadrige\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TemplatePlatform
 *
 * @ORM\Table(name="TEMPLATE_PLATFORM", indexes={@ORM\Index(name="tpl_id", columns={"tpl_id"}), @ORM\Index(name="tpl_plateforme_id", columns={"tpl_plateforme_id"})})
 * @ORM\Entity(repositoryClass="Kadrige\ApiBundle\Repository\TemplatePlatformRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TemplatePlatform
{

    /**
     * @var integer
     *
     * @ORM\Column(name="tpl_user_id", type="string", length=255, nullable=false)
     * @Groups({"simple_template","extended_template","list_template","simple_platform","extended_platform"})
     * @Assert\NotBlank(groups={"registration"},
     *    message = "The userId '{{ value }}' is not a valid userId (should be not blank)",
     * )
     * @Assert\Email(
     *    message = "The userId '{{ value }}' is not a valid email",
     *    groups={"registration"}
     * )
     */
    private $userId;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="tpl_plf_date_creation", type="datetime", nullable=true)
    * @Groups({"simple_template","extended_template","list_template","simple_platform","extended_platform"})
    */
    private $dateCreation;

    /**
    * @var integer
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="IDENTITY")
    */
    private $id;

    /**
    * @var \Kadrige\ApiBundle\Entity\Template
    *
    * @ORM\ManyToOne(targetEntity="Kadrige\ApiBundle\Entity\Template", inversedBy="templatePlatforms")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="tpl_id", referencedColumnName="tpl_id")
    * })
    * @Groups({"simple_template","extended_template","list_template","simple_platform","extended_platform"})
    */
    private $template;

    /**
    * @var \Kadrige\ApiBundle\Entity\Platform
    *
    * @ORM\ManyToOne(targetEntity="Kadrige\ApiBundle\Entity\Platform", inversedBy="templatePlatforms")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="tpl_plateforme_id", referencedColumnName="plf_id")
    * })
    * @Groups({"simple_template","extended_template","simple_platform","extended_platform"})
    */
    private $plateforme;

    /**
     * Set userId
     *
     * @param integer $userId
     * @return TemplatePlatform
     */
    public function setUserId($tplUserId)
    {
        $this->userId = $tplUserId;
        return $this;
    }

    /**
     * Get userId
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /* Set dateCreation
     *
     * @param \DateTime $tplPlfDateCreation
     * @return TemplatePlatform
     * @ORM\PostPersist
     */
    public function setDateCreation()
    {
        $this->dateCreation = new \DateTime();
        return $this;
    }

    /**
     * Get dateCreation
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set template
     *
     * @param \Kadrige\ApiBundle\Entity\Template $template
     * @return TemplatePlatform
     */
    public function setTpl(\Kadrige\ApiBundle\Entity\Template $tpl = null)
    {
        $this->template = $tpl;

        return $this;
    }

    /**
     * Get template
     * @return \Kadrige\ApiBundle\Entity\Template
     */
    public function getTpl()
    {
        return $this->template;
    }

    /**
     * Set tplPlateforme
     *
     * @param \Kadrige\ApiBundle\Entity\Platform $plateforme
     * @return TemplatePlatform
     */
    public function setPlateforme(\Kadrige\ApiBundle\Entity\Platform $tplPlateforme = null)
    {
        $this->plateforme = $tplPlateforme;

        return $this;
    }

    /**
     * Get plateforme
     * @return \Kadrige\ApiBundle\Entity\Platform
     */
    public function getPlateforme()
    {
        return $this->plateforme;
    }


}
