<?php

namespace Kadrige\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Template
 *
 * @ORM\Table(name="TEMPLATE", uniqueConstraints={@ORM\UniqueConstraint(name="tpl_name", columns={"tpl_name"}), @ORM\UniqueConstraint(name="tpl_branch_name", columns={"tpl_branch_name"})})
 * @ORM\Entity(repositoryClass="Kadrige\ApiBundle\Repository\TemplateRepository")
 */
class Template
{

    /**
     * @var string
     *
     * @ORM\Column(name="tpl_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(groups={"creation"},
     *    message = "The name '{{ value }}' is not a valid name (should be not blank)"
     * )
     * @Groups({"simple_template","extended_template","list_template","simple_platform","extended_platform"})
     *
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="tpl_branch_name", type="string", length=255, nullable=false)
     * @Groups({"simple_template","extended_template","list_template","simple_platform","extended_platform"})
     */
    private $branchName;

    /**
     * @var string
     *
     * @ORM\Column(name="tpl_disclaimer", type="text", length=65535, nullable=false)
     * @Groups({"simple_template","extended_template","simple_platform","extended_platform"})
     * @Assert\NotBlank(groups={"registration"},
     *    message = "The disclaimer '{{ value }}' is not a valid disclaimer (should be not blank)"
     * )
     */
    private $disclaimer;

    /**
     * @var string
     *
     * @ORM\Column(name="tpl_adresse", type="text", length=65535, nullable=false)
     * @Groups({"simple_template","extended_template","simple_platform","extended_platform"})
     * @Assert\NotBlank(groups={"registration"},
     *    message = "The adresse '{{ value }}' is not a valid adresse (should be not blank)",
     * )
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="tpl_url", type="text", length=65535, nullable=false)
     * @Groups({"simple_template","extended_template","simple_platform","extended_platform"})
     * @Assert\NotBlank(groups={"registration"},
     *    message = "The url '{{ value }}' is not a valid url (should be not blank)",
     * )
     * @Assert\Url(
     *    message = "The url '{{ value }}' is not a valid url",
     *    groups={"registration"}
     * )
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="tpl_color", type="string", length=10, nullable=false)
     * @Groups({"simple_template","extended_template","simple_platform","extended_platform"})
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="tpl_colorb", type="string", length=10, nullable=false)
     * @Groups({"simple_template","extended_template","simple_platform","extended_platform"})
     */
    private $colorb;


    /**
     * @var integer
     *
     * @ORM\Column(name="tpl_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"simple_template","extended_template","simple_platform","extended_platform"})
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Kadrige\ApiBundle\Entity\TemplatePlatform", mappedBy="template")
     * @Groups({"simple_template","extended_template"})
     */
    private $templatePlatforms;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->templatePlatforms = new ArrayCollection();
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Template
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set tplPlateformeId
     *
     * @param integer $tplPlateformeId
     *
     * @return Template
     */
    public function setPlateformeId($tplPlateformeId)
    {
        $this->plateformeId = $tplPlateformeId;

        return $this;
    }

    /**
     * Get tplPlateformeId
     *
     * @return integer
     */
    public function getPlateformeId()
    {
        return $this->plateformeId;
    }

    /**
     * Set tplName
     *
     * @param string $tplName
     *
     * @return Template
     */
    public function setName($tplName)
    {
        $this->name = $tplName;

        return $this;
    }

    /**
     * Get tplName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tplBranchName
     *
     * @param string $tplBranchName
     *
     * @return Template
     */
    public function setBranchName($tplBranchName)
    {
        $this->branchName = $tplBranchName;

        return $this;
    }

    /**
     * Get tplBranchName
     *
     * @return string
     */
    public function getBranchName()
    {
        return $this->branchName;
    }

    /**
     * Set tplDateCreation
     *
     * @param \DateTime $tplDateCreation
     *
     * @return Template
     */
    public function setDateCreation($tplDateCreation)
    {
        $this->dateCreation = $tplDateCreation;

        return $this;
    }

    /**
     * Get tplDateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set tplDisclaimer
     *
     * @param string $tplDisclaimer
     *
     * @return Template
     */
    public function setDisclaimer($tplDisclaimer)
    {
        $this->disclaimer = $tplDisclaimer;

        return $this;
    }

    /**
     * Get tplDisclaimer
     *
     * @return string
     */
    public function getDisclaimer()
    {
        return $this->disclaimer;
    }

    /**
     * Set tplAdresse
     *
     * @param string $tplAdresse
     *
     * @return Template
     */
    public function setAdresse($tplAdresse)
    {
        $this->adresse = $tplAdresse;

        return $this;
    }

    /**
     * Get tplAdresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set tplUrl
     *
     * @param string $tplUrl
     *
     * @return Template
     */
    public function setUrl($tplUrl)
    {
        $this->url = $tplUrl;

        return $this;
    }

    /**
     * Get tplUrl
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set color
     *
     * @param string $tplColor
     *
     * @return Template
     */
    public function setColor($tplColor)
    {
        $this->color = $tplColor;

        return $this;
    }

    /**
     * Get tplColor
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set colorb
     *
     * @param string $tplColorb
     *
     * @return Template
     */
    public function setColorb($tplColorb)
    {
        $this->colorb = $tplColorb;

        return $this;
    }

    /**
     * Get tplColorb
     *
     * @return string
     */
    public function getColorb()
    {
        return $this->colorb;
    }

    /**
     * Get tplId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @VirtualProperty
     * @SerializedName("background")
     * @Groups({"extended_template"})
     */
    public function getFullPathBackground()
    {
       return $_SERVER["HTTP_HOST"]."/web/bundles/kadrigeapi/templates/86-custom/content/img/fond.jpg";
    }

    /**
     * @VirtualProperty
     * @SerializedName("logo")
     * @Groups({"extended_template"})
     */
    public function getFullPathLogo()
    {
       return $_SERVER["HTTP_HOST"]."/web/bundles/kadrigeapi/templates/86-custom/content/img/logo.jpg";
    }

    /**
     * @VirtualProperty
     * @SerializedName("cartouche")
     * @Groups({"extended_template"})
     */
    public function getFullPathCartouche()
    {
       return $_SERVER["HTTP_HOST"]."/web/bundles/kadrigeapi/templates/86-custom/content/img/fond_cartouche.jpg";
    }

    /**
     * @VirtualProperty
     * @SerializedName("sass")
     * @Groups({"extended_template"})
     */
    public function getFullPathCss()
    {
       return $_SERVER["HTTP_HOST"]."/web/bundles/kadrigeapi/templates/86-custom/content/sass/_global.scss";
    }


    /**
     * Add templatePlatform
     *
     * @param \Kadrige\ApiBundle\Entity\TemplatePlatform $templatePlatform
     *
     * @return Template
     */
    public function addTemplatePlatform(\Kadrige\ApiBundle\Entity\TemplatePlatform $templatePlatform)
    {
        $this->templatePlatforms[] = $templatePlatform;

        return $this;
    }

    /**
     * Remove templatePlatform
     *
     * @param \Kadrige\ApiBundle\Entity\TemplatePlatform $templatePlatform
     */
    public function removeTemplatePlatform(\Kadrige\ApiBundle\Entity\TemplatePlatform $templatePlatform)
    {
        $this->templatePlatforms->removeElement($templatePlatform);
    }

    /**
     * Get templatePlatforms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTemplatePlatforms()
    {
        return $this->templatePlatforms;
    }
}
