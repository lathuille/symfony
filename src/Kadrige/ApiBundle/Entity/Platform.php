<?php

namespace Kadrige\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Platform
 *
 * @ORM\Table(name="PLATFORM")
 * @ORM\Entity(repositoryClass="Kadrige\ApiBundle\Repository\PlatformRepository")
 */
class Platform
{
    /**
     * @var string
     * @ORM\Column(name="plf_name", type="string", length=255, nullable=false)
     * @Groups({"simple_platform","extended_platform","simple_template","extended_template"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="plf_client_name", type="string", length=255, nullable=false)
     * @Groups({"simple_platform","extended_platform","simple_template","extended_template"})
     * @Assert\NotBlank(groups={"registration"},
     *    message = "The client '{{ value }}' is not a valid client (should be not blank)",
     * )
     * @Assert\NotNull(groups={"registration"},
     *    message = "The client '{{ value }}' is not a valid client (should be not null)",
     * )
     */
    private $clientName;


    /**
     * @var integer
     *
     * @Groups({"simple_platform","extended_platform","simple_template","extended_template"})
     * @Assert\Regex("/^\d+$/",
     *    message = "The plateformeId '{{ value }}' is not a valid plateformeId (should be integer)",
     *    groups={"registration"}
     * )
     *
     * @ORM\Column(name="plf_mpa_id", type="integer", nullable=false)
     */
    private $mpaId;

    /**
     * @var integer
     *
     * @Groups({"simple_platform","extended_platform","simple_template","extended_template"})
     * @ORM\Column(name="plf_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

     /**
      * @ORM\OneToMany(targetEntity="Kadrige\ApiBundle\Entity\TemplatePlatform", mappedBy="plateforme")
      * @Groups({"simple_platform","extended_platform"})
      */
     private $templatePlatforms;

     /**
      * constructor
      */
     public function __construct()
     {
         $this->templatePlatforms = new ArrayCollection();
     }

    /**
     * Set name
     *
     * @param string $plfName
     * @return Platform
     */
    public function setName($plfName)
    {
        $this->name = $plfName;

        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set clientName
     *
     * @param integer $plfClientName
     * @return Platform
     */
    public function setClientName($plfClientName)
    {
        $this->clientName = $plfClientName;

        return $this;
    }

    /**
     * Get clientName
     * @return integer
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * Set mpaId
     *
     * @param integer $plfMpaId
     * @return Platform
     */
    public function setMpaId($plfMpaId)
    {
        $this->mpaId = $plfMpaId;

        return $this;
    }

    /**
     * Get mpaId
     * @return integer
     */
    public function getMpaId()
    {
        return $this->mpaId;
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Add templatePlatform
    *
    * @param \Kadrige\ApiBundle\Entity\TemplatePlatform $templatePlatform
    * @return Platform
    */
    public function addTemplatePlatform(\Kadrige\ApiBundle\Entity\TemplatePlatform $templatePlatform)
    {
       $this->templatePlatforms[] = $templatePlatform;
       return $this;
    }

    /**
    * Remove templatePlatform
    * @param \Kadrige\ApiBundle\Entity\TemplatePlatform $templatePlatform
    */
    public function removeTemplatePlatform(\Kadrige\ApiBundle\Entity\TemplatePlatform $templatePlatform)
    {
       $this->templatePlatforms->removeElement($templatePlatform);
    }

    /**
    * Get templatePlatforms
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getTemplatePlatforms()
    {
       return $this->templatePlatforms;
    }

}
